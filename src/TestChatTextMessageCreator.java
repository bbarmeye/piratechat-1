import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;


public class TestChatTextMessageCreator {
	ChatMessageCreator cmc;
	Connection con;
	Session sesh;
	Message msg;
	String sender, type, str;
	
	@Before
	public void setUp() throws JMSException{
		sender = "Bob";
		type = "type";
		str = "This is the message.";
		
		String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
	    con = connectionFactory.createConnection();
	    con.start();
	    sesh = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
	}

	@Test
	public void testCreateChatMessage() throws JMSException{
		TextMessage tm = null;
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getStringProperty(sender), "Bob");
        assertEquals(tm.getIntProperty(type), 1);
        assertEquals(tm.getText(), str);
        assertFalse(tm == null);
	}

	@Test
	public void testIsUsable() {
		Message good_msg1 = null;
		good_msg1 = (Message) sesh.createTextMessage("howdy");
		assertTrue(isUsable(good_msg1));
		
		Message good_msg2 = (Message) sesh.createTextMessage();
		assertTrue(isUsable(good_msg2));
		
		ObjectMessage bad_msg = sesh.createObjectMessage("yoooooo");
		assertFalse(isUsable(bad_msg));
	}

	@Test
	public void testGetChatMessageType() throws JMSException{
		TextMessage tm;
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getIntProperty(type), 1);
		tm.setIntProperty(type, 0);
		assertFalse(tm.getIntProperty(type) == 1);
	}

	@Test
	public void testGetChatMessageSender() throws JMSException{
		msg = cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(msg.getStringProperty(sender), "Bob");
		sender = null;
		assertFalse(msg.getStringProperty(sender).equals("Bob"));
	}

	@Test
	public void testGetChatMessageText() throws JMSException{
		TextMessage tm;
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getText(), str);
		str = null;
		assertFalse(tm.getText().equals(str));
	}

}

