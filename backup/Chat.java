import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Chat implements ActionListener, WindowListener, MessageListener {

   ActiveMQConnectionFactory connectionFactory;
   Connection connection;
   Session session;
   MessageProducer msgProducer;
   MessageConsumer msgConsumer;
   Topic topic;

   boolean connected = false;

   String name, hostName, topicName, outgoingMsgTypeString;
   int outgoingMsgType;

   Frame frame;
   ChatPanel scp;
   ChatDialog scd = null;
   MenuItem clearItem, exitItem;
   Button sendB, connectB, cancelB;

   ChatMessageCreator outgoingMsgCreator;

   ChatMessageCreator txtMsgCreator, objMsgCreator, mapMsgCreator,
         bytesMsgCreator, streamMsgCreator;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public Chat() {
      name = System.getProperty("user.name", "johndoe");
      topicName = "defaulttopic";

      try {
         hostName = InetAddress.getLocalHost().getHostName();
      } catch (Exception e) {
         hostName = "localhost";
      }
   }

   public ChatMessageCreator getMessageCreator() {
      txtMsgCreator = new ChatTextMessageCreator();
      return (txtMsgCreator);
   }

   public void actionPerformed(ActionEvent e) {
      Object obj = e.getSource();

      if (obj == sendB) {
         sendNormalMessage();
      } else if (obj == clearItem) {
         scp.clear();
      } else if (obj == exitItem) {
         exit();
      }
   }

   public void windowClosing(WindowEvent e) {
      e.getWindow().dispose();
   }

   public void windowClosed(WindowEvent e) {
      exit();
   }

   public void windowActivated(WindowEvent e) {
   }

   public void windowDeactivated(WindowEvent e) {
   }

   public void windowDeiconified(WindowEvent e) {
   }

   public void windowIconified(WindowEvent e) {
   }

   public void windowOpened(WindowEvent e) {
   }

   public void onMessage(Message msg) {
      String sender, msgText;
      int type;
      ChatMessageCreator inboundMsgCreator;

      inboundMsgCreator = getMessageCreator();

      if (inboundMsgCreator == null) {
         errorMessage("Message received is not supported ! ");
         return;
      }

      /*
       * Need to fetch msg values in this order.
       */
      type = inboundMsgCreator.getChatMessageType(msg);
      sender = inboundMsgCreator.getChatMessageSender(msg);
      msgText = inboundMsgCreator.getChatMessageText(msg);

      if (type == ChatMessageTypes.BADTYPE) {
         errorMessage("Message received in wrong format ! ");
         return;
      }

      scp.newMessage(sender, type, msgText);
   }

   /*
    * END INTERFACE MessageListener
    */

   /*
    * Popup the ChatDialog to query the user for the chat user name and chat
    * topic.
    */
   private void queryForChatNames() {
      if (scd == null) {
         scd = new ChatDialog(frame);
         connectB = scd.getConnectButton();
         connectB.addActionListener(this);
         cancelB = scd.getCancelButton();
         cancelB.addActionListener(this);
      }

      scd.setChatUserName(name);
      scd.setChatTopicName(topicName);
      scd.setVisible(true);
   }

   /*
    * Performs the actual chat connect. The createChatSession() method does the
    * real work here, creating: Connection Session Topic MessageConsumer
    * MessageProducer
    */
   private void doConnect() {
      if (connectedToChatSession())
         return;

      outgoingMsgCreator = getMessageCreator();

      if (createChatSession(topicName) == false) {
         errorMessage("Unable to create Chat session.  "
               + "Please verify a broker is running");
         return;
      }
      setConnectedToChatSession(true);

      scp.setUserName(name);
      scp.setDestName(topicName);
      scp.setHostName(hostName);
      scp.setEnabled(true);
   }

   /*
    * Disconnects from chat session. destroyChatSession() performs the JMS
    * cleanup.
    */
   private void doDisconnect() {
      if (!connectedToChatSession())
         return;

      destroyChatSession();

      setConnectedToChatSession(false);

      scp.setEnabled(false);
   }

   /*
    * These methods set/return a flag that indicates whether the application is
    * currently involved in a chat session.
    */
   private void setConnectedToChatSession(boolean b) {
      connected = b;
   }

   private boolean connectedToChatSession() {
      return (connected);
   }

   /*
    * Exit application. Does some cleanup if necessary.
    */
   private void exit() {
      doDisconnect();
      System.exit(0);
   }

   /*
    * Create the application GUI.
    */
   private void initGui() {

      frame = new Frame(" Chat");
      frame.addWindowListener(this);
      MenuBar menubar = createMenuBar();
      frame.setMenuBar(menubar);

      scp = new ChatPanel();
      scp.setUserName(name);
      scp.setDestName(topicName);
      scp.setHostName(hostName);
      sendB = scp.getSendButton();
      sendB.addActionListener(this);

      frame.add(scp);
      frame.pack();
      frame.setVisible(true);

      scp.setEnabled(false);
   }

   /*
    * Create menubar for application.
    */
   private MenuBar createMenuBar() {
      MenuBar mb = new MenuBar();
      Menu chatMenu;

      chatMenu = (Menu) mb.add(new Menu("Connection Spec"));

      clearItem = (MenuItem) chatMenu.add(new MenuItem("Clear Messages"));
      exitItem = (MenuItem) chatMenu.add(new MenuItem("Log Out"));

      clearItem.addActionListener(this);
      exitItem.addActionListener(this);

      return (mb);
   }

   /*
    * Send message using text that is currently in the ChatPanel object. The
    * text message is obtained via scp.getMessage()
    * 
    * An object of type ChatObjMessage is created containing the typed text. A
    * JMS ObjectMessage is used to encapsulate this ChatObjMessage object.
    */
   private void sendNormalMessage() {
      Message msg;

      if (!connectedToChatSession()) {
         errorMessage("Cannot send message: Not connected to chat session!");

         return;
      }

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name,
               ChatMessageTypes.NORMAL, scp.getMessage());

         msgProducer.send(msg);
         scp.setMessage("");
         scp.requestFocus();
      } catch (Exception ex) {
         errorMessage("Caught exception while sending NORMAL message: " + ex);
      }
   }

   /*
    * Send a message to the chat session to inform people we just joined the
    * chat.
    */
   private void sendJoinMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name,
               ChatMessageTypes.JOIN, null);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending JOIN message: " + ex);
      }
   }

   /*
    * Send a message to the chat session to inform people we are leaving the
    * chat.
    */
   private void sendLeaveMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name,
               ChatMessageTypes.LEAVE, null);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending LEAVE message: " + ex);
      }
   }

   /*
    * JMS initialization. This is simply creating the ConnectionFactory.
    */
   private void initializeActiveMq(String args[]) {
      try {
         //String url = "failover:(tcp://localhost:61616)?maxReconnectDelay=5000";
         String url = args[0];
         name = args[1];
         topicName = args[2];
         connectionFactory = new ActiveMQConnectionFactory(url);
         doConnect();
         
      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
      }
   }

   /*
    * Create 'chat session'. This involves creating: Connection Session Topic
    * MessageConsumer MessageProducer
    */
   private boolean createChatSession(String topicStr) {
      try {
         /*
          * Create the connection...
          */
         connection = connectionFactory.createConnection();

         /*
          * Not transacted Auto acknowledegement
          */
         session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
         //Only users on same topic can chat together
         topic = session.createTopic(topicStr);
         msgProducer = session.createProducer(topic);
         /*
          * Non persistent delivery
          */
         msgProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
         msgConsumer = session.createConsumer(topic);
         msgConsumer.setMessageListener(this);

         connection.start();

         sendJoinMessage();

         return true;

      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
         e.printStackTrace();
         return false;
      }
   }

   /*
    * Destroy/close 'chat session'.
    */
   private void destroyChatSession() {
      try {
         sendLeaveMessage();

         msgConsumer.close();
         msgProducer.close();
         session.close();
         connection.close();

         topic = null;
         msgConsumer = null;
         msgProducer = null;
         session = null;
         connection = null;

      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
      }

   }

   /*
    * Display error. Right now all we do is dump to stderr.
    */
   private void errorMessage(String s) {
      System.err.println(s);
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    */
   public static void main(String[] args) {
      
      Chat sc = new Chat();

      sc.initGui();
      sc.initializeActiveMq(args);
   }

}
